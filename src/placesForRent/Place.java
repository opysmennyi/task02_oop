package placesForRent;

import java.util.Objects;

public class Place {
    int square;
    String locatiol;
    int price;
    int childhood;
    int clinic;
    int school;
    int center;
    int flor;
    String accomodation;
    int parkingPlace;
    int comunalPayment;

    public Place(int square, String locatiol, int price, Integer childhood, Integer clinic, Integer school, Integer center, int flor, String accomodation, int parkingPlace, int comunalPayment) {
        this.square = square;
        this.locatiol = locatiol;
        this.price = price;
        this.childhood = childhood;
        this.clinic = clinic;
        this.school = school;
        this.center = center;
        this.flor = flor;
        this.accomodation = accomodation;
        this.parkingPlace = parkingPlace;
        this.comunalPayment = comunalPayment;
    }

    public Place() {
    }

    public int getSquare() {
        return square;
    }

    public void setSquare(int square) {
        this.square = square;
    }

    public String getLocatiol() {
        return locatiol;
    }

    public void setLocatiol(String locatiol) {
        this.locatiol = locatiol;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Integer getChildhood() {
        return childhood;
    }

    public void setChildhood(Integer childhood) {
        this.childhood = childhood;
    }

    public Integer getClinic() {
        return clinic;
    }

    public void setClinic(Integer clinic) {
        this.clinic = clinic;
    }

    public Integer getSchool() {
        return school;
    }

    public void setSchool(Integer school) {
        this.school = school;
    }

    public Integer getCenter() {
        return center;
    }

    public void setCenter(Integer center) {
        this.center = center;
    }

    public int getFlor() {
        return flor;
    }

    public void setFlor(int flor) {
        this.flor = flor;
    }

    public String getAccomodation() {
        return accomodation;
    }

    public void setAccomodation(String accomodation) {
        this.accomodation = accomodation;
    }

    public int getParkingPlace() {
        return parkingPlace;
    }

    public void setParkingPlace(int parkingPlace) {
        this.parkingPlace = parkingPlace;
    }

    public int getComunalPayment() {
        return comunalPayment;
    }

    public void setComunalPayment(int comunalPayment) {
        this.comunalPayment = comunalPayment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Place place = (Place) o;
        return square == place.square &&
                price == place.price &&
                flor == place.flor &&
                parkingPlace == place.parkingPlace &&
                comunalPayment == place.comunalPayment &&
                Objects.equals(locatiol, place.locatiol) &&
                Objects.equals(childhood, place.childhood) &&
                Objects.equals(clinic, place.clinic) &&
                Objects.equals(school, place.school) &&
                Objects.equals(center, place.center) &&
                Objects.equals(accomodation, place.accomodation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(square, locatiol, price, childhood, clinic, school, center, flor, accomodation, parkingPlace, comunalPayment);
    }

    @Override
    public String toString() {
        return "Place{" +
                "square=" + square +
                ", locatiol='" + locatiol + '\'' +
                ", price=" + price +
                ", childhood='" + childhood + '\'' +
                ", clinic='" + clinic + '\'' +
                ", school='" + school + '\'' +
                ", center='" + center + '\'' +
                ", flor=" + flor +
                ", accomodation='" + accomodation + '\'' +
                ", parkingPlace=" + parkingPlace +
                ", comunalPayment=" + comunalPayment +
                '}';
    }
}
