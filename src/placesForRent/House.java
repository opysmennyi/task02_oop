package placesForRent;

import java.util.Objects;

public class House extends ForLive {

    String backyard;
    int squareBackyard;



    public House(int square, String locatiol, int price, Integer childhood, Integer clinic, Integer school, Integer center, int flor, String accomodation, int parkingPlace, int comunalPayment, String backyard, int squareBackyard) {
        super(square, locatiol, price, childhood, clinic, school, center, flor, accomodation, parkingPlace, comunalPayment);
        this.backyard = backyard;
        this.squareBackyard = squareBackyard;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        House house = (House) o;
        return squareBackyard == house.squareBackyard &&
                Objects.equals(backyard, house.backyard);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), backyard, squareBackyard);
    }

    @Override
    public String toString() {
        return "House{" +
                "backyard='" + backyard + '\'' +
                ", squareBackyard=" + squareBackyard +
                ", kitchen=" + kitchen +
                ", ckitchenSquare=" + ckitchenSquare +
                ", bedroom=" + bedroom +
                ", bathroom=" + bathroom +
                ", heating=" + heating +
                ", square=" + square +
                ", locatiol='" + locatiol + '\'' +
                ", price=" + price +
                ", childhood='" + childhood + '\'' +
                ", clinic='" + clinic + '\'' +
                ", school='" + school + '\'' +
                ", center='" + center + '\'' +
                ", flor=" + flor +
                ", accomodation='" + accomodation + '\'' +
                ", parkingPlace=" + parkingPlace +
                ", comunalPayment=" + comunalPayment +
                "} " + super.toString();
    }
}
