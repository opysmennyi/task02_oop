package placesForRent;

import java.util.Objects;

public class Office extends ForWork {

    String reception;
    String guardians;
    int toilets;

    public Office(int square, String locatiol, int price, Integer childhood, Integer clinic, Integer school, Integer center, int flor, String accomodation, int parkingPlace, int comunalPayment, String reception, String guardians, int toilets) {
        super(square, locatiol, price, childhood, clinic, school, center, flor, accomodation, parkingPlace, comunalPayment);
        this.reception = reception;
        this.guardians = guardians;
        this.toilets = toilets;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Office office = (Office) o;
        return toilets == office.toilets &&
                Objects.equals(reception, office.reception) &&
                Objects.equals(guardians, office.guardians);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), reception, guardians, toilets);
    }

    @Override
    public String toString() {
        return "Office{" +
                "reception='" + reception + '\'' +
                ", guardians='" + guardians + '\'' +
                ", toilets=" + toilets +
                ", square=" + square +
                ", locatiol='" + locatiol + '\'' +
                ", price=" + price +
                ", childhood='" + childhood + '\'' +
                ", clinic='" + clinic + '\'' +
                ", school='" + school + '\'' +
                ", center='" + center + '\'' +
                ", flor=" + flor +
                ", accomodation='" + accomodation + '\'' +
                ", parkingPlace=" + parkingPlace +
                ", comunalPayment=" + comunalPayment +
                "} " + super.toString();
    }
}
