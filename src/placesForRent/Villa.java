package placesForRent;

import java.util.Objects;

public class Villa extends ForLive{

    int pool;
    int sauna;


    public Villa(int square, String locatiol, int price, Integer childhood, Integer clinic, Integer school, Integer center, int flor, String accomodation, int parkingPlace, int comunalPayment, int pool, int sauna) {
        super(square, locatiol, price, childhood, clinic, school, center, flor, accomodation, parkingPlace, comunalPayment);
        this.pool = pool;
        this.sauna = sauna;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Villa villa = (Villa) o;
        return pool == villa.pool &&
                sauna == villa.sauna;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), pool, sauna);
    }

    @Override
    public String toString() {
        return "Villa{" +
                "pool=" + pool +
                ", sauna=" + sauna +
                ", kitchen=" + kitchen +
                ", ckitchenSquare=" + ckitchenSquare +
                ", bedroom=" + bedroom +
                ", bathroom=" + bathroom +
                ", heating=" + heating +
                ", square=" + square +
                ", locatiol='" + locatiol + '\'' +
                ", price=" + price +
                ", childhood=" + childhood +
                ", clinic=" + clinic +
                ", school=" + school +
                ", center=" + center +
                ", flor=" + flor +
                ", accomodation='" + accomodation + '\'' +
                ", parkingPlace=" + parkingPlace +
                ", comunalPayment=" + comunalPayment +
                "} " + super.toString();
    }
}
