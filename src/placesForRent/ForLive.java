package placesForRent;

import java.util.Objects;

public class ForLive extends Place{

    int kitchen;
    int ckitchenSquare;
    int bedroom;
    int bathroom;
    String heating;


    public ForLive(int square, String locatiol, int price, Integer childhood, Integer clinic, Integer school, Integer center, int flor, String accomodation, int parkingPlace, int comunalPayment, int kitchen, int ckitchenSquare, int bedroom, int bathroom, String heating) {
        super(square, locatiol, price, childhood, clinic, school, center, flor, accomodation, parkingPlace, comunalPayment);
        this.kitchen = kitchen;
        this.ckitchenSquare = ckitchenSquare;
        this.bedroom = bedroom;
        this.bathroom = bathroom;
        this.heating = heating;
    }

    public ForLive(int kitchen, int ckitchenSquare, int bedroom, int bathroom, String heating) {
        this.kitchen = kitchen;
        this.ckitchenSquare = ckitchenSquare;
        this.bedroom = bedroom;
        this.bathroom = bathroom;
        this.heating = heating;
    }

    public ForLive() {
    }

    public ForLive(int square, String locatiol, int price, Integer childhood, Integer clinic, Integer school, Integer center, int flor, String accomodation, int parkingPlace, int comunalPayment) {
        super(square, locatiol, price, childhood, clinic, school, center, flor, accomodation, parkingPlace, comunalPayment);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ForLive forLive = (ForLive) o;
        return kitchen == forLive.kitchen &&
                ckitchenSquare == forLive.ckitchenSquare &&
                bedroom == forLive.bedroom &&
                bathroom == forLive.bathroom &&
                heating == forLive.heating;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), kitchen, ckitchenSquare, bedroom, bathroom, heating);
    }

    @Override
    public String toString() {
        return "ForLive{" +
                "kitchen=" + kitchen +
                ", ckitchenSquare=" + ckitchenSquare +
                ", bedroom=" + bedroom +
                ", bathroom=" + bathroom +
                ", heating=" + heating +
                ", square=" + square +
                ", locatiol='" + locatiol + '\'' +
                ", price=" + price +
                ", childhood='" + childhood + '\'' +
                ", clinic='" + clinic + '\'' +
                ", school='" + school + '\'' +
                ", center='" + center + '\'' +
                ", flor=" + flor +
                ", accomodation='" + accomodation + '\'' +
                ", parkingPlace=" + parkingPlace +
                ", comunalPayment=" + comunalPayment +
                "} " + super.toString();
    }
}
