package placesForRent;

import java.util.Objects;

public class Appartment extends ForLive {

    String rentOrNo;

    public Appartment(int square, String locatiol, int price, Integer childhood, Integer clinic, Integer school, Integer center, int flor, String accomodation, int parkingPlace, int comunalPayment, int kitchen, int ckitchenSquare, int bedroom, int bathroom, String heating, String rentOrNo) {
        super(square, locatiol, price, childhood, clinic, school, center, flor, accomodation, parkingPlace, comunalPayment, kitchen, ckitchenSquare, bedroom, bathroom, heating);
        this.rentOrNo = rentOrNo;
    }

    public Appartment(int kitchen, int ckitchenSquare, int bedroom, int bathroom, String heating, String rentOrNo) {
        super(kitchen, ckitchenSquare, bedroom, bathroom, heating);
        this.rentOrNo = rentOrNo;
    }

    public Appartment(String rentOrNo) {
        this.rentOrNo = rentOrNo;
    }

    public Appartment(int square, String locatiol, int price, Integer childhood, Integer clinic, Integer school, Integer center, int flor, String accomodation, int parkingPlace, int comunalPayment, String rentOrNo) {
        super(square, locatiol, price, childhood, clinic, school, center, flor, accomodation, parkingPlace, comunalPayment);
        this.rentOrNo = rentOrNo;
    }

    public String getRentOrNo() {

        return rentOrNo;
    }

    public void setRentOrNo(String rentOrNo) {
        this.rentOrNo = rentOrNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Appartment that = (Appartment) o;
        return Objects.equals(rentOrNo, that.rentOrNo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), rentOrNo);
    }

    @Override
    public String toString() {
        return "Appartment{" +
                "rentOrNo='" + rentOrNo + '\'' +
                ", kitchen=" + kitchen +
                ", ckitchenSquare=" + ckitchenSquare +
                ", bedroom=" + bedroom +
                ", bathroom=" + bathroom +
                ", heating='" + heating + '\'' +
                ", square=" + square +
                ", locatiol='" + locatiol + '\'' +
                ", price=" + price +
                ", childhood=" + childhood +
                ", clinic=" + clinic +
                ", school=" + school +
                ", center=" + center +
                ", flor=" + flor +
                ", accomodation='" + accomodation + '\'' +
                ", parkingPlace=" + parkingPlace +
                ", comunalPayment=" + comunalPayment +
                "} " + super.toString();
    }
}