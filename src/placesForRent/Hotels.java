package placesForRent;

import java.util.Objects;

public class Hotels extends ForLive{

    String brackfest;
    int cleaningTimes;



    public Hotels(int square, String locatiol, int price, Integer childhood, Integer clinic, Integer school, Integer center, int flor, String accomodation, int parkingPlace, int comunalPayment, String brackfest, int cleaningTimes) {
        super(square, locatiol, price, childhood, clinic, school, center, flor, accomodation, parkingPlace, comunalPayment);
        this.brackfest = brackfest;
        this.cleaningTimes = cleaningTimes;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Hotels hotels = (Hotels) o;
        return cleaningTimes == hotels.cleaningTimes &&
                Objects.equals(brackfest, hotels.brackfest);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), brackfest, cleaningTimes);
    }

    @Override
    public String toString() {
        return "Hotels{" +
                "brackfest='" + brackfest + '\'' +
                ", cleaningTimes=" + cleaningTimes +
                ", kitchen=" + kitchen +
                ", ckitchenSquare=" + ckitchenSquare +
                ", bedroom=" + bedroom +
                ", bathroom=" + bathroom +
                ", heating=" + heating +
                ", square=" + square +
                ", locatiol='" + locatiol + '\'' +
                ", price=" + price +
                ", childhood='" + childhood + '\'' +
                ", clinic='" + clinic + '\'' +
                ", school='" + school + '\'' +
                ", center='" + center + '\'' +
                ", flor=" + flor +
                ", accomodation='" + accomodation + '\'' +
                ", parkingPlace=" + parkingPlace +
                ", comunalPayment=" + comunalPayment +
                "} " + super.toString();
    }

}
